import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import numpy as np

# Apply affine transformation then rectified linear unit
class ReLULayer(nn.Module):
    # Set up a ReLU layer for ease of use
    def __init__(self, input_dim, output_dim):
        super(ReLULayer, self).__init__()
        self.linear = nn.Linear(input_dim, output_dim) # Just one affine transformation

    def forward(self, x):
        return F.relu(self.linear(x)) # Apply the affine transformation and then the ReLU

# Apply a ReLU layer then add the input to the result
class ResidualLayer(nn.Module):
    # Set up a residual unit (res = ReLU(x + A_out * ReLU(A_in * x + b_in) + b_out) ) with width hidden units
    def __init__(self, input_dim, width):
        super(ResidualLayer, self).__init__()
        self.inner = ReLULayer(input_dim, width) # The inner affine transformation and function application
        self.outer = nn.Linear(width, input_dim) # The outer affine transformation

    def forward(self, x):
        return F.relu(x + self.outer(self.inner(x))) # Apply the transform and add the original

# Basic width x depth network
class NeuralNetwork(nn.Module):
    def __init__(self, input_dim, output_dim, depth, width, learning_rate=0.001):
        super(NeuralNetwork, self).__init__()
        self.width = width
        self.depth = depth

        layers = []
        layers.append(ReLULayer(input_dim, width)) # Setup initial hidden layer

        for i in range(1, depth):
            layers.append(ReLULayer(width, width)) # Setup hidden layers

        layers.append(nn.Linear(width, output_dim)) # Setup output layer (linear)

        self.layers = nn.ModuleList(layers)

        self.optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)

        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        self.num_params = sum([np.prod(p.size()) for p in model_parameters])

    def forward(self, x):
        out = x

        for i in range(self.depth + 1):
            out = self.layers[i](out) # Apply hidden layers

        return out

    def train(self, X_train, y_train, loss_function):
        pred = self(X_train) # Predict from our inputs
        loss = loss_function(pred, y_train) # Calculate the loss
        # Backpropgate
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss


# Residual network with width nodes per residual unit
class ResidualNeuralNetwork(nn.Module):
    def __init__(self, input_dim, output_dim, depth, width=1, learning_rate=0.001):
        super(ResidualNeuralNetwork, self).__init__()
        self.depth = depth
        self.input_dim = input_dim

        layers = []
        for i in range(depth):
            layers.append(ResidualLayer(input_dim, width)) # Set up all the hidden layers

        layers.append(nn.Linear(input_dim, output_dim)) # Setup the linear output layer

        self.layers = nn.ModuleList(layers)

        self.optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)

        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        self.num_params = sum([np.prod(p.size()) for p in model_parameters])

    def forward(self, x):
        out = x

        for i in range(len(self.layers)):
            out = self.layers[i](out) # Apply hidden layers

        return out

    def train(self, X_train, y_train, loss_function):
        pred = self(X_train) # Predict from our inputs
        loss = loss_function(pred, y_train) # Calculate the loss
        # Backpropgate
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss
        
class ResidualNeuralNetworkWide(nn.Module):
    def __init__(self, input_dim, output_dim, depth, width=1, learning_rate=0.001):
        super(ResidualNeuralNetworkWide, self).__init__()
        self.depth = depth
        self.input_dim = input_dim

        layers = []
        layers.append(nn.Linear(input_dim, width))
        for i in range(depth):
            layers.append(ResidualLayer(width, width)) # Set up all the hidden layers

        layers.append(nn.Linear(width, output_dim)) # Setup the linear output layer

        self.layers = nn.ModuleList(layers)

        self.optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)

        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        self.num_params = sum([np.prod(p.size()) for p in model_parameters])

    def forward(self, x):
        out = x

        for i in range(len(self.layers)):
            out = self.layers[i](out) # Apply hidden layers

        return out

    def train(self, X_train, y_train, loss_function):
        pred = self(X_train) # Predict from our inputs
        loss = loss_function(pred, y_train) # Calculate the loss
        # Backpropgate
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss
