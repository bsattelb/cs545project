{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PyTorch:  neural networks and their building blocks"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.nn.functional as F\n",
    "import torch.optim as optim\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Warmup:  Linear regression\n",
    "\n",
    "As a warmup, let's write a solver for linear regression using PyTorch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# define feature matrix and labels\n",
    "x = torch.randn(10, 5, requires_grad=False)\n",
    "y = torch.randn(10, 3, requires_grad=False)\n",
    "# a weight matrix\n",
    "w = torch.randn(5, 3, requires_grad=True)\n",
    "\n",
    "print(w.grad)  # None\n",
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The heart of the matter is the definition of our loss function, which is the root mean squared error for linear regression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "loss = torch.mean((y - x @ w) ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All we need to do next is to compute its gradient, which we get for free for every PyTorch tensor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# calculate the gradient\n",
    "loss.backward(retain_graph=True)\n",
    "# manually apply the gradient\n",
    "w.data -= 0.01 * w.grad.data\n",
    "# manually zero gradients after update\n",
    "w.grad.zero_()\n",
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can make the computation easier using the built in optimizers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = torch.randn(10, 5, requires_grad=False)\n",
    "y = torch.randn(10, 3, requires_grad=False)\n",
    "\n",
    "w = torch.randn(5, 3, requires_grad=True)\n",
    "b = torch.randn(3, requires_grad=True)\n",
    "\n",
    "learning_rate = 0.1\n",
    "loss_fn = torch.nn.MSELoss()\n",
    "optimizer = torch.optim.SGD([w,b], lr=learning_rate)\n",
    "for step in range(10000):\n",
    "    # compute model predictions\n",
    "    pred = x @ w + b\n",
    "    # compute the loss\n",
    "    loss = loss_fn(pred, y)\n",
    "    if step%1000==0:\n",
    "        print (loss.item())\n",
    "    # manually zero all previous gradients\n",
    "    optimizer.zero_grad()\n",
    "    # calculate new gradients\n",
    "    loss.backward()\n",
    "    # perform an optimization step\n",
    "    optimizer.step()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[ 0.4830,  0.6686,  0.4467],\n",
       "        [-1.0320,  1.1213,  1.3144]], grad_fn=<ThAddmmBackward>)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# a linear mapping from R^5 to R^3\n",
    "linear = nn.Linear(5, 3)  \n",
    "# some data...\n",
    "X = torch.randn(2, 5)\n",
    "# apply the mapping...\n",
    "linear(X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<generator object Module.parameters at 0x10654aeb8>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "Parameter containing:\n",
       "tensor([0.1062, 0.3992, 0.0269], requires_grad=True)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "linear.parameters()\n",
    "linear.bias"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need some nonlinearities...\n",
    "\n",
    "Pytorch provides the standard nonlinearities:\n",
    "$\\tanh(x), \\sigma(x), \\text{ReLU}(x)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([[-1.0269, -0.8036],\n",
      "        [ 0.2149,  0.0842]])\n",
      "tensor([[0.0000, 0.0000],\n",
      "        [0.2149, 0.0842]])\n"
     ]
    }
   ],
   "source": [
    "data = torch.randn(2, 2)\n",
    "print(data)\n",
    "print(F.relu(data))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Softmax and Probabilities\n",
    "\n",
    "The $\\text{softmax}(x)$ function is another non-linearity, but\n",
    "it is special in that it usually is the last operation in a\n",
    "network. It takes in a vector of real numbers and\n",
    "returns a probability distribution, and therefore useful for multi-class classification. Its definition is as follows. Let\n",
    "$x$ be a vector of real numbers. Then the $i$th component of\n",
    "$\\text{softmax}(x)$ is\n",
    "\n",
    "\\begin{align}\\frac{\\exp(x_i)}{\\sum_j \\exp(x_j)}\\end{align}\n",
    "\n",
    "It should be clear that the output is a probability distribution: each\n",
    "element is non-negative and the sum over all components is 1.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before creating a simple classifier in PyTorch we need some data.  It's easy to convert one of the scikit-learn datasets into PyTorch tensors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "\n",
    "from sklearn.datasets import load_breast_cancer\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "X,y = load_breast_cancer(return_X_y=True)\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1)\n",
    "\n",
    "X_train = torch.tensor(X_train, dtype=torch.float)\n",
    "y_train = torch.tensor(y_train, dtype=torch.long)\n",
    "X_test = torch.tensor(X_test, dtype=torch.float)\n",
    "y_test = torch.tensor(y_test, dtype=torch.long)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "num_features = X_train.shape[1]\n",
    "num_classes = 2\n",
    "# hyper-parameters \n",
    "num_epochs = 5000\n",
    "learning_rate = 0.00001"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "model = nn.Sequential(\n",
    "    nn.Linear(num_features, num_classes),\n",
    "    nn.LogSoftmax()\n",
    ")\n",
    "loss_function = nn.NLLLoss()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch [0/5000], Loss: 79.6137\n",
      "Epoch [500/5000], Loss: 0.3970\n",
      "Epoch [1000/5000], Loss: 0.2464\n",
      "Epoch [1500/5000], Loss: 0.2113\n",
      "Epoch [2000/5000], Loss: 0.1980\n",
      "Epoch [2500/5000], Loss: 0.1915\n",
      "Epoch [3000/5000], Loss: 0.1876\n",
      "Epoch [3500/5000], Loss: 0.1850\n",
      "Epoch [4000/5000], Loss: 0.1830\n",
      "Epoch [4500/5000], Loss: 0.1815\n"
     ]
    }
   ],
   "source": [
    "for epoch in range(num_epochs):\n",
    "    pred = model(X_train)\n",
    "    loss = loss_function(pred, y_train)\n",
    "    # backward and optimize\n",
    "    model.zero_grad()\n",
    "    loss.backward()\n",
    "    with torch.no_grad():\n",
    "        for param in model.parameters():\n",
    "            param -= learning_rate * param.grad    \n",
    "    if epoch %500 == 0 :\n",
    "        print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch, num_epochs, loss.item()))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9181286549707602"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "with torch.no_grad():\n",
    "    output = model(X_test)\n",
    "    predicted = torch.argmax(output.data, dim=1)\n",
    "    (y_test==predicted).sum().item()/len(y_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "num_features = X_train.shape[1]\n",
    "num_classes = 2\n",
    "# hyper-parameters \n",
    "num_epochs = 5000\n",
    "learning_rate = 0.00001\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class LogisticRegression(nn.Module):\n",
    "    def __init__(self, num_features, num_classes):\n",
    "        # calls the init function of nn.Module.  Dont get confused by syntax\n",
    "        super(LogisticRegression, self).__init__()\n",
    "        self.linear = nn.Linear(num_features, num_classes)\n",
    "    def forward(self, x):\n",
    "        # Pass the input through the linear layer,\n",
    "        # then pass that through softmax.\n",
    "        return F.log_softmax(self.linear(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "num_features = X_train.shape[1]\n",
    "num_classes = 2\n",
    "# hyper-parameters \n",
    "num_epochs = 5000\n",
    "learning_rate = 0.00001\n",
    "model = LogisticRegression(num_features, num_classes)\n",
    "loss_function = nn.NLLLoss()\n",
    "optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)\n",
    "#optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch [0/5000], Loss: 160.8974\n",
      "Epoch [500/5000], Loss: 0.4091\n",
      "Epoch [1000/5000], Loss: 0.3101\n",
      "Epoch [1500/5000], Loss: 0.2537\n",
      "Epoch [2000/5000], Loss: 0.2196\n",
      "Epoch [2500/5000], Loss: 0.2066\n",
      "Epoch [3000/5000], Loss: 0.2018\n",
      "Epoch [3500/5000], Loss: 0.1989\n",
      "Epoch [4000/5000], Loss: 0.1968\n",
      "Epoch [4500/5000], Loss: 0.1951\n"
     ]
    }
   ],
   "source": [
    "for epoch in range(num_epochs):\n",
    "    pred = model(X_train)\n",
    "    loss = loss_function(pred, y_train)\n",
    "    # backward and optimize\n",
    "    optimizer.zero_grad()\n",
    "    loss.backward()\n",
    "    optimizer.step()\n",
    "    if epoch %500 == 0 :\n",
    "        print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch, num_epochs, loss.item()))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9064327485380117"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "with torch.no_grad():\n",
    "    output = model(X_test)\n",
    "    predicted = torch.argmax(output.data, dim=1)\n",
    "    (y_test==predicted).sum().item()/len(y_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch [0/5000], Loss: 28.8451\n",
      "Epoch [500/5000], Loss: 10.0051\n",
      "Epoch [1000/5000], Loss: 0.6491\n",
      "Epoch [1500/5000], Loss: 0.5519\n",
      "Epoch [2000/5000], Loss: 0.4772\n",
      "Epoch [2500/5000], Loss: 0.4080\n",
      "Epoch [3000/5000], Loss: 0.3500\n",
      "Epoch [3500/5000], Loss: 0.3043\n",
      "Epoch [4000/5000], Loss: 0.2656\n",
      "Epoch [4500/5000], Loss: 0.2523\n"
     ]
    }
   ],
   "source": [
    "class NeuralNetwork(nn.Module):\n",
    "    def __init__(self, num_features, num_hidden, num_classes):\n",
    "        super(NeuralNetwork, self).__init__()\n",
    "        self.linear1 = nn.Linear(num_features, num_hidden)\n",
    "        self.linear2 = nn.Linear(num_hidden, num_classes)\n",
    "    def forward(self, x):\n",
    "        hidden=F.relu(self.linear1(x))\n",
    "        return F.log_softmax(self.linear2(hidden))\n",
    "    \n",
    "num_features = X_train.shape[1]\n",
    "num_classes = 2\n",
    "# hyper-parameters \n",
    "num_epochs = 5000\n",
    "learning_rate = 0.00001\n",
    "num_hidden = 30\n",
    "model = NeuralNetwork(num_features, num_hidden, num_classes)\n",
    "loss_function = nn.NLLLoss()\n",
    "#optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)\n",
    "optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)  \n",
    "\n",
    "for epoch in range(num_epochs):\n",
    "    pred = model(X_train)\n",
    "    loss = loss_function(pred, y_train)\n",
    "    # backward and optimize\n",
    "    optimizer.zero_grad()\n",
    "    loss.backward()\n",
    "    optimizer.step()\n",
    "    if epoch %500 == 0 :\n",
    "        print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch, num_epochs, loss.item()))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9181286549707602"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "with torch.no_grad():\n",
    "    output = model(X_test)\n",
    "    predicted = torch.argmax(output.data, dim=1)\n",
    "    (y_test==predicted).sum().item()/len(y_test)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
